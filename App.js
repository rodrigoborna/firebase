import React from 'react';
import { Text, TextInput, TouchableOpacity, View,Button,Alert} from 'react-native';
import styles from './styles.js';
import {insertUser, insertMoviment, selectUser} from './firebase/crud';

  insertUser (1, 'Rodrigo', '123456', 'rodrigo.nascimento@outlook.com')

  insertMoviment(1, 'Almoço', 23.9, '08/02/2021', '09/03/2021', 'Alimentação', 'Santander', null)

  insertMoviment(1, 'Jantar', 50, '08/02/2021', '09/03/2021', 'Alimentação', 'Santander', null)

 insertUser (2, 'Fabiana', '123456', 'fabiana.nascimento@outlook.com')

  insertMoviment(1, 'Café', 5.5, '08/02/2021', '09/03/2021', 'Social', 'Santander', null)

  insertMoviment(2, 'Suco', 9, '08/02/2021', '09/03/2021', 'Social', 'Santander', null)

//var user = console.log(selectUser(1));

export default function App() {

  return (

    <View style={styles.registerExpense}>
        <Button
        title="Press me"
        onPress={() => selectUser(1)}
      />
    </View>
  );
}