import firebase from './firebaseConfig';

// -users
//     - name
//     - password
//     - ...
//     - movement (collection)

//Caminho pai do documento
const userWay = 'users/';
//Caminho filho do documento 
const movementWay = 'movement/';

export async function insertUser(userId, name, password, email, movement) {
    firebase.database().ref(userWay + userId).set(
        {
            name: name,
            password: password,
            email: email,
        }
    )
}

export async function insertMoviment(userId, description, value, movementDate
    , realizedDate, category, wallet, tag) {

    var movementKey = firebase.database().ref().child(userWay + userId).child(movementWay).push().key

    firebase.database().ref(userWay + userId).child(movementWay + movementKey).set(
        {
            description: description,
            value: value,
            movementDate: movementDate,
            realizedDate: realizedDate,
            category: category,
            wallet: wallet,
            tag: tag
        }
    )
}

export function selectUser(userId) {
    //return firebase.database().ref().child('users/' + userId).ref.once('value').then((snapshot) => {
    //   var user = snapshot;

        return firebase.database().ref('users/'+ userId).on('value', (data) => {
            console.log('Objeto completo >>>>>>');
            console.log(data.toJSON());
            console.log('Somente o Email >>>>>>');
            console.log(data.child('email').toJSON());
            console.log('Somente o Movimento >>>>>>');
            console.log(data.child('movement').toJSON());
        
    }
    );
}


