import { StyleSheet } from "react-native";

const styles = StyleSheet.create(
    {
        registerExpense: {
            backgroundColor: '#c7cfb7',
            margin: 10,
            borderRadius: 12,
            borderWidth: 5,
            borderColor: '#c7cfb7',
            flex: 1,
            elevation: 3,
            height: 300
        },

        lineOne: {
            flex: 0.25
        },

        lineTwo: {
            flex: 0.75
        },

        input: {
            height: 45,
            backgroundColor: '#c7cfb7',
            alignSelf: 'stretch',
            borderColor: '#EEE',
            borderWidth: 1,
            paddingHorizontal: 20,
            marginBottom: 10
        },


        button: {
            width: "80%",
            paddingVertical: 20,
            backgroundColor: "#9dad7f",
            borderRadius: 50,
            alignItems: 'center',
            alignSelf: 'center'
        },
    }
);

export default styles;
